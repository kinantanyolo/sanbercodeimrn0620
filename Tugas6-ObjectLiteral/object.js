//Soal 1
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)
function arrayToObject(arr) {
    var array = [];
    for(var i=0; i<arr.length; i++){
        array.push({});
        array[i].firstName =arr[i][0];
        array[i].lastName =arr[i][1];
        array[i].gender =arr[i][2];
        if(arr[i][3] && arr[i][3]<=thisYear){
            array[i].age = thisYear - arr[i][3];
        }else{
            array[i].age = "Invalid Birth Year";
        }
        console.log(String(i+1) + ". " + array[i].firstName + " " + array[i].lastName + " :");
        console.log(array[i]);
    }
    return array;
}
 // Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
// Error case 
arrayToObject([])

//Soal 2

function shoppingTime(memberId, money) {
    var current = money;
    var list = [];
    if(!memberId || memberId==""){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }else{
        if(current<50000){
            return "Mohon maaf, uang tidak cukup";
        }
        if(current>=1500000){
            list.push("Sepatu Stacattu");
            current-=1500000;
        }
        if(current>=500000){
            list.push("Baju Zoro");
            current-=500000;
        }
        if(current>=250000){
            list.push("Baju H&N");
            current-=250000;
        }
        if(current>=175000){
            list.push("Sweater Uniklooh");
            current-=175000;
        }
        if(current>=50000){
            list.push("Casing Handphone");
            current-=50000;
        }
    }
    var obj = {
        memberId: memberId,
        money: money,
        listPurchased: list,
        changeMoney: current
    }
    return obj;
}
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

//Soal 3
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var array = [];
    for(var i=0; i<arrPenumpang.length; i++){
        array.push({
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: 0
        });
        var x=0;
        while(rute[x]!=arrPenumpang[i][1]){
            x++;
        }
        while(rute[x]!=arrPenumpang[i][2]){
            x++;
            array[i].bayar+=2000;
        }
    }
    return array;
}
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));