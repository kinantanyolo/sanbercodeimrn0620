//Soal1
class Score {
    constructor(email, points, subject){
        this.email = email;
        this.subject = subject;
        this.points = points
    }
    average() {
        let mean = 0;
        for(let i=0; i<this.points.length; i++){
            mean = mean + this.points[i];
        }
        mean = mean/this.points.length;
        return mean;
    }
}

//Soal2
const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]
function viewScores(data, subject) {
    let x=0;
    while(data[0][x] != subject){
        x++;
    }
    let scores = [];
    for(let i=1; i<data.length; i++){
        newScore = new Score(data[i][0], data[i][x], subject);
        scores.push(newScore);
    }
    console.log(scores);
}
// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

//Soal3
function recapScores(data) {
    for(let i=1; i<data.length; i++){
        let [email, ...points] = data[i];
        newScore = new Score(email, points);
        let mean = newScore.average();
        let predikat = (mean>90 ? "honour" : mean>80 ? "graduate" : mean>70 ? "participant" : "");
        console.log(`${i}. Email: ${email}`);
        console.log(`Rata-rata: ${mean}`);
        console.log(`Predikat: ${predikat}`);
        console.log();
    }
}
recapScores(data);