//Soal 1
var nama = "Merlin"
var peran = "Penyihir"

if(nama == ''){
    console.log("Nama harus diisi!");
}else{
    if(peran == ''){
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
    }else{
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        if(peran == 'Penyihir'){
            console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
        }else if(peran == 'Guard'){
            console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
        }else if(peran == 'Werewolf'){
            console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!");
        }
    }
}

//Soal 2
var tanggal = 6; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 12; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2001; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch(bulan){
    case 1: {bulan = 'Januari'}
    case 2: {bulan = 'Februari'}
    case 3: {bulan = 'Maret'}
    case 4: {bulan = 'April'}
    case 5: {bulan = 'Mei'}
    case 6: {bulan = 'Juni'}
    case 7: {bulan = 'Juli'}
    case 8: {bulan = 'Agustus'}
    case 9: {bulan = 'September'}
    case 10: {bulan = 'Oktober'}
    case 11: {bulan = 'November'}
    case 12: {bulan = 'Desember'}
}
console.log(String(tanggal)+' '+bulan+' '+String(tahun));