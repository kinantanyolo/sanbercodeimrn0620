function balikString(start){
    var finish="";
    for(var i=0; i<start.length; i++){
        finish = start[i]+finish;
    }
    return finish;
}
function palindrome(string){
    var answer=true;
    for(var i=0; i<string.length; i++){
        if(string[i] != string[string.length-1-i]){
            answer=false;
        }
    }
    return answer;
}
function bandingkan(num1, num2){
    if(!num1){
        return -1;
    }else if(!num2){
        if(parseInt(num1)<0){
            return -1;
        }else{
            return parseInt(num1);
        }
    }else{
        if(parseInt(num1)<0 || parseInt(num2)<0){
            return -1;
        }else{
            if(parseInt(num1) > parseInt(num2)){
                return parseInt(num1);
            }else if(parseInt(num1) < parseInt(num2)){
                return parseInt(num2);
            }else{
                return -1;
            }
        }
    }
}

// TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18