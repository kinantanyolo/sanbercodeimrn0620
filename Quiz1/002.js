function AscendingTen(num) {
    if(num){
        var answer = String(num);
        for(var i=1; i<10; i++){
            answer = answer + " " + String(num+i);
        }
        return answer;
    }else{
        return -1;
    }
}

function DescendingTen(num) {
    if(num){
        var answer = String(num);
        for(var i=1; i<10; i++){
            answer = answer + " " + String(num-i);
        }
        return answer;
    }else{
        return -1;
    }
}

function ConditionalAscDesc(reference, check) {
    if(reference && check){
        check = (check%2)*2-1;
        var answer = String(reference);
        for(var i=1; i<10; i++){
            answer = answer + " " + String(reference+(i*check));
        }
        return answer;
    }else{
        return -1;
    }
}

function ularTangga() {
    for(var i=10; i>0; i--){
        var string = String(10*i);
        if(i%2==0){
            for(var j=1; j<10; j++){
                string = string + " " + String(10*i-j);
            }
        }else{
            for(var j=1; j<10; j++){
                string =String(10*i-j) + " " + string;
            }
        }
        console.log(string);
    }
    return "";
}

// TEST CASES Ascending Ten
console.log(AscendingTen(11))
console.log(AscendingTen(21))
console.log(AscendingTen())

// TEST CASES Descending Ten
console.log(DescendingTen(100))
console.log(DescendingTen(10))
console.log(DescendingTen())

// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8))
console.log(ConditionalAscDesc(81, 1))
console.log(ConditionalAscDesc(31))
console.log(ConditionalAscDesc())

// TEST CASE Ular Tangga
console.log(ularTangga()) 