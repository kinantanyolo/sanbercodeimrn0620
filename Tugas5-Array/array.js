//Soal 1
function range(startNum, finishNum){
    if(startNum && finishNum){
        var array = [];
        var x=startNum;
        if(startNum <= finishNum){
            while(x<=finishNum){
                array.push(x);
                x++;
            }
        }else{
            while(x>=finishNum){
                array.push(x);
                x--;
            }
        }
        return array;
    }else{
        return -1;
    }
}
console.log(range(1, 10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54, 50));
console.log(range());

//Soal 2
function rangeWithStep(startNum, finishNum, step){
    if(startNum && finishNum){
        var array = [];
        var x=startNum;
        if(startNum <= finishNum){
            while(x<=finishNum){
                array.push(x);
                x+=step;
            }
        }else{
            while(x>=finishNum){
                array.push(x);
                x-=step;
            }
        }
        return array;
    }else{
        return -1;
    }
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

//Soal 3
function sum(a, b, c){
    var total=0;
    var step=1;
    if(c){
        step=c;
    }
    var array=rangeWithStep(a, b, step);
    if(array == -1){
        if(a){
            total=a;
        }
    }else{
        for(var i=0; i<array.length; i++){
            total+=array[i];
        }
    }
    return total;
}
console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

//Soal 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
function dataHandling(){
    for(var i=0; i<input.length; i++){
        console.log("Nomor ID: "+input[i][0]);
        console.log("Nama Lengkap: "+input[i][1]);
        console.log("TTL: "+input[i][2]+" "+input[i][3]);
        console.log("Hobi: "+input[i][4]);
        console.log();
    }
}
dataHandling();

//Soal 5
function balikKata(start){
    var finish="";
    for(var i=0; i<start.length; i++){
        finish = start[i]+finish;
    }
    return finish;
}
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

//Soal 6
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(input){
    var name = input[1]+"Elsharawy";
    var province = "Provinsi "+input[2]
    input.splice(1, 2, name, province);
    input.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(input);
    var tanggal = input[3].split("/");
    switch(parseInt(tanggal[1])){
        case 1: {console.log('Januari'); break;}
        case 2: {console.log('Februari'); break;}
        case 3: {console.log('Maret'); break;}
        case 4: {console.log('April'); break;}
        case 5: {console.log('Mei'); break;}
        case 6: {console.log('Juni'); break;}
        case 7: {console.log('Juli'); break;}
        case 8: {console.log('Agustus'); break;}
        case 9: {console.log('September'); break;}
        case 10: {console.log('Oktober'); break;}
        case 11: {console.log('November'); break;}
        case 12: {console.log('Desember'); break;}
    }
    tanggal2=[];
    tanggal2.push(tanggal[0]);
    tanggal2.push(tanggal[1]);
    tanggal2.push(tanggal[2]);
    tanggal2.sort(function(a, b){return b-a});
    console.log(tanggal2);
    console.log(tanggal.join("-"));
    console.log(name.slice(0, 14));
}
dataHandling2(input);