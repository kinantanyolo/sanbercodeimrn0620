//Soal 1
console.log("LOOPING PERTAMA");
var a = 0;
while(a<20){
    a+=2;
    console.log(String(a) + ' - I love coding');
}
console.log("LOOPING KEDUA");
while(a>0){
    console.log(String(a) + ' - I will become a mobile developer');
    a-=2;
}

//Soal 2
for(var i=1; i<=20; i++){
    if(i%2==0){
        console.log(String(i) + ' - Berkualitas');
    }else{
        if(i%3==0){
            console.log(String(i) + ' - I Love Coding');
        }else{
            console.log(String(i) + ' - Santai');
        }
    }
}

//Soal 3
var row='';
var baris = 4;
var kolom = 8;
for(var i=0; i<kolom; i++){
    row=row+'#';
}
for(var i=0; i<baris; i++){
    console.log(row);
}

//Soal 4
var row='';
for(var i=0; i<7; i++){
    row=row+'#';
    console.log(row);
}

//Soal 5
var row='';
for(var i=0; i<8; i++){
    row=''
    for(var j=0; j<8; j++){
        if((i+j)%2==0){
            row=row+' ';
        }else{
            row=row+'#';
        }
    }
    console.log(row);
}