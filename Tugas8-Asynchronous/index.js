var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Tulis code untuk memanggil function readBooks di sini
function callBooks(curTime, curBook){
    readBooks(curTime, books[curBook], function(time){
        if(time<curTime){
            curTime = time;
            curBook++;
            if(curBook<books.length){
                callBooks(curTime, curBook);
            }
        }
    })
}
callBooks(10000, 0);