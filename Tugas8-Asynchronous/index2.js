var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
curTime = 10000;
curBooks = 0;
function readBooks() {
    readBooksPromise(curTime, books[curBooks])
        .then(function(sisaWaktu){
            curTime = sisaWaktu;
            curBooks++;
            if(curBooks < books.length){
                readBooks();
            }
        })
        .catch(function(sisaWaktu){
            console.log("");
        })
}
readBooks();