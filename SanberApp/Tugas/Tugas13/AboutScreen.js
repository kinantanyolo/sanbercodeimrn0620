import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.title}>
                    <TouchableOpacity>
                        <Icon name="home" size={40} style={{paddingLeft: 20}}/>
                    </TouchableOpacity>
                    <Text style={{fontSize: 48, paddingLeft: 80}}>About</Text>
                </View>
                <View style={styles.body}>
                    <View style={styles.topBody}>
                        <Image source={require('./images/FotoDiri.png')} style={{width:150, height:295}}/>
                        <View style={styles.profile}>
                            <View style={{backgroundColor: "#C4C4C4", height: 80}}>
                                <Text style={{fontSize: 24}}>Kinantan Arya Bagaspati</Text>
                            </View>
                            <Text>React Native Developer</Text>
                        </View>
                    </View>
                    <View style={styles.bottomBody}>
                        <View style={styles.bottomTitle}>
                            <Text>Portofolio</Text>
                        </View>
                        <View style={styles.bottomContent}>
                            <View style={{alignItems: "center", justifyContent:"space-between"}}>
                                <Image source={require('./images/github.png')} style={{width:50, height:50, borderRadius: 25}}/>
                                <Text>@kinantanbagaspati</Text>
                            </View>
                            <View style={{alignItems: "center", justifyContent:"space-between"}}>
                                <Image source={require('./images/gitlab.png')} style={{width:50, height:50}}/>
                                <Text>@kinantanyolo</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.bottomBody}>
                        <View style={styles.bottomTitle}>
                            <Text>Hubungi saya</Text>
                        </View>
                        <View style={styles.bottomContent}>
                        <View style={{alignItems: "center", justifyContent:"space-between"}}>
                                <Image source={require('./images/instagram.png')} style={{width:50, height:50}}/>
                                <Text>kinantanbagaspati</Text>
                            </View>
                            <View style={{alignItems: "center", justifyContent:"space-between"}}>
                                <Image source={require('./images/facebook.png')} style={{width:50, height:50}}/>
                                <Text>Kinantan Bagaspati</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
  
  const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    title: {
        height: 94,
        borderBottomWidth: 1,
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    body: {
        flex: 1,
        alignItems: 'center'
    },
    topBody: {
        flexDirection: "row",
        justifyContent: "center"
    },
    bottomBody: {
        backgroundColor: '#C4C4C4',
        width: 326,
        height: 150,
        marginBottom: 20
    },
    profile: {
        justifyContent: "center",
        width: 176,
    },
    bottomTitle: {
        height: 30,
        borderBottomWidth: 1
    },
    bottomContent:{
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        flex: 1
    }
})