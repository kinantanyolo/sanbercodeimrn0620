import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export const Login = () => {
    return (
        <View style={styles.container}>
            <Text style={{fontSize: 48, paddingTop: 30}}>SanberApp</Text>
            <Image source={require('./images/logo.png')} style={{width:200, height:200}}/>
            <Text style={{fontSize: 36, marginBottom: 10}}>Login</Text>
            <View style={styles.input}>
                <Text>Email / Username :</Text>
                <View style={{flex: 1, backgroundColor: '#C4C4C4'}}></View>
            </View>
            <View style={styles.input}>
                <Text>Password :</Text>
                <View style={{flex: 1, backgroundColor: '#C4C4C4'}}></View>
            </View>
            <TouchableOpacity>
                <View style={styles.button}>
                    <Text style={{fontSize: 24, color: 'white'}}>Login</Text>
                </View>
            </TouchableOpacity>
            <Text style={{fontSize: 24, marginBottom: 20, marginTop: 50}}>Belum punya akun?</Text>
            <TouchableOpacity> 
                <View style={styles.button}>
                    <Text style={{fontSize: 24, color: 'white'}}>Register</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    input: {
        height: 62,
        width: 310,
        marginBottom: 20
    },
    button: {
        backgroundColor: '#288906',
        borderRadius: 20,
        height: 40,
        width: 110,
        alignItems: "center"
    }
})