import React from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

import {About} from './AboutScreen';
import {Add} from './AddScreen';
import {Login} from './LoginScreen';
import {Project} from './ProjectScreen';
import {Skill} from './SkillScreen';

const Tabs = createBottomTabNavigator();

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Skill" component={SkillStackScreen}/>
    <Tabs.Screen name="Project" component={ProjectStackScreen}/>
    <Tabs.Screen name="Add" component={AddStackScreen}/>
  </Tabs.Navigator>
)

const AboutStack = createStackNavigator();
const AboutStackScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="About" component={About}/>
  </AboutStack.Navigator>
)

const AddStack = createStackNavigator();
const AddStackScreen = () => (
  <AddStack.Navigator>
    <AddStack.Screen name="Add" component={Add}/>
  </AddStack.Navigator>
)

const LoginStack = createStackNavigator();
const LoginStackScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name="Login" component={Login}/>
  </LoginStack.Navigator>
)

const ProjectStack = createStackNavigator();
const ProjectStackScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="Project" component={Project}/>
  </ProjectStack.Navigator>
)

const SkillStack = createStackNavigator();
const SkillStackScreen = () => (
  <SkillStack.Navigator>
    <SkillStack.Screen name="Skill" component={Skill}/>
  </SkillStack.Navigator>
)

const Drawer = createDrawerNavigator();

export default () => (
  <NavigationContainer>
    <Drawer.Navigator>
      <Drawer.Screen name="Login" component = {LoginStackScreen}/>
      <Drawer.Screen name="About" component = {AboutStackScreen}/>
      <Drawer.Screen name="Skill" component = {TabsScreen}/>
    </Drawer.Navigator>
  </NavigationContainer>
);
