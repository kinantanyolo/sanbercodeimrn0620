import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export const Add = () => {
    return (
        <View style={styles.container}>
            <Text>Halaman Tambah</Text>
        </View>
    )
}
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})