import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export const Skill = () => {
    return (
        <View style={styles.container}>
            <View style={styles.title}>
                <Text style={{fontSize: 48}}>Home</Text>
                <TouchableOpacity>
                    <View style={styles.button}>
                        <Text style={{fontSize: 24, color: 'white'}}>About</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={styles.button}>
                        <Text style={{fontSize: 24, color: 'white'}}>Logout</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <Text style={{alignItems: 'center', marginTop: 20, fontSize: 24}}>Welcome User</Text>
            <View style={styles.titleSkill}>
                <Text style={{fontSize: 24, marginRight: 100}}>Your Skills :</Text>
                <TouchableOpacity>
                    <View style={styles.button}>
                        <Text style={{fontSize: 24, color: 'white'}}>Add +</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.skill}>
                <View style={{flexDirection: 'row', justifyContent: 'flex-start', width: 340}}>
                    <Image source={require('./images/ReactNative.png')} style={{width:100, height:100}}/>
                    <View style={{justifyContent: "space-evenly", paddingLeft: 10}}>
                        <Text style={{fontSize:18}}>Skill Name: React Native</Text>
                        <Text style={{fontSize:18}}>Mastery: 50%</Text>
                    </View>
                </View>
                <Text style={{fontSize:18}}>Skill Type :</Text>
                <View style={{flexDirection: "row", justifyContent: 'space-between', width: 340}}>
                    <View style={styles.notchosen}>
                        <Text style={{fontSize:16}}>Language</Text>
                    </View>
                    <View style={styles.chosen}>
                        <Text style={{color: 'white', fontSize: 16}}>Library</Text>
                    </View>
                    <View style={styles.notchosen}>
                        <Text style={{fontSize:16}}>Technology</Text>
                    </View>
                </View>
                <Text style={{fontSize:18}}>Skill level :</Text>
                <View style={{flexDirection: "row", justifyContent: 'space-between', width: 340}}>
                    <View style={styles.chosen}>
                        <Text style={{color: 'white', fontSize:16}}>Basic</Text>
                    </View>
                    <View style={styles.notchosen}>
                        <Text style={{fontSize: 16}}>Intermediate</Text>
                    </View>
                    <View style={styles.notchosen}>
                        <Text style={{fontSize:16}}>Advance</Text>
                    </View>
                </View>
            </View>
            <View style={styles.skill}>
                <View style={{flexDirection: 'row', justifyContent: 'flex-start', width: 340}}>
                    <Image source={require('./images/cplus.png')} style={{width:100, height:100}}/>
                    <View style={{justifyContent: "space-evenly", paddingLeft: 10}}>
                        <Text style={{fontSize:18}}>Skill Name: C++</Text>
                        <Text style={{fontSize:18}}>Mastery: 40%</Text>
                    </View>
                </View>
                <Text style={{fontSize:18}}>Skill Type :</Text>
                <View style={{flexDirection: "row", justifyContent: 'space-between', width: 340}}>
                    <View style={styles.chosen}>
                        <Text style={{color: 'white', fontSize:16}}>Language</Text>
                    </View>
                    <View style={styles.notchosen}>
                        <Text style={{fontSize: 16}}>Library</Text>
                    </View>
                    <View style={styles.notchosen}>
                        <Text style={{fontSize:16}}>Technology</Text>
                    </View>
                </View>
                <Text style={{fontSize:18}}>Skill level :</Text>
                <View style={{flexDirection: "row", justifyContent: 'space-between', width: 340}}>
                    <View style={styles.notchosen}>
                        <Text style={{fontSize:16}}>Basic</Text>
                    </View>
                    <View style={styles.chosen}>
                        <Text style={{color: 'white', fontSize: 16}}>Intermediate</Text>
                    </View>
                    <View style={styles.notchosen}>
                        <Text style={{fontSize:16}}>Advance</Text>
                    </View>
                </View>
            </View>
        </View>
        
    )
}
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    title: {
        height: 94,
        width: 400,
        borderBottomWidth: 1,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
    },
    button: {
        backgroundColor: '#288906',
        borderRadius: 20,
        height: 40,
        width: 110,
        alignItems: "center",
        justifyContent: 'center'
    },
    titleSkill: {
        height: 40,
        width: 340,
        borderBottomWidth: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingTop: 30,
        paddingBottom: 30
    },
    skill: {
        height: 260,
        width: 340,
        backgroundColor: '#C3F0BC',
        justifyContent: "space-between",
        marginTop: 15,
        alignItems: 'center'
    },
    chosen: {
        height: 40,
        width: 110,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#288906'
    },
    notchosen: {
        height: 40,
        width: 110,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#A4A4A4'
    }
})